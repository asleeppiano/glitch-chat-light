import React from "react";
import { storiesOf } from "@storybook/react";
import '../src/index.scss'
import '../src/App.scss'
import Room from "../src/components/room/room";
import Message from "../src/components/message/message";
import {action} from '@storybook/addon-actions'

const d = new Date();

storiesOf("Room", module)
  .add("default", () => (
    <Room
      onClick={action('clicked')}
      room="roomid"
      lastMessage={{ text: "last message", name: "andrey" }}
      date={d}
    />
  ));

const text = `asdfseafhas;df
asfasdfsefsaddf adfasdf eadf asdf 
fasefsdf`;

storiesOf("Message", module)
  .add("sender", () => (
    <Message text={text} name="andrey" date={d} type="sender" />
  ))
  .add("recipient", () => (
    <Message text={text} name="vadim" date={d} type="recipient" />
  ));
