// Require the framework and instantiate it
const nanoidGenerate = require('nanoid/generate')
import * as fastify from 'fastify'
import * as socketio from 'socket.io'
import { Server, IncomingMessage, ServerResponse } from 'http'
import * as mongo from 'mongodb'
import * as dotenv from 'dotenv'
import * as path from 'path'

dotenv.config()

const mongoClient = mongo.MongoClient
const dbUsername = process.env.DB_USERNAME
const dbPassword = process.env.DB_PASSWORD

const dbUrl =
  process.env.NODE_ENV === 'production'
    ? `mongodb+srv://${dbUsername}:${dbPassword}@glitchmessenger-0ipt0.azure.mongodb.net?retryWrites=true&w=majority`
    : 'mongodb://localhost:27017'

const dbName = 'messenger'

const dbClient = new mongoClient(dbUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

let db: mongo.Db = null

const server: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify({
  logger: { prettyPrint: true },
})
const io = socketio(server.server)

server.register(require('fastify-cors'), {})

server.register(require('fastify-static'), {
  root: path.join(__dirname + '/../..', 'build'),
})

server.get('/', function(req, reply: any) {
  reply.sendFile('index.html')
})

enum MessageType {
  MESSAGE = 'message',
  CHIP = 'chip',
}

interface IMessage {
  user: string
  text: string
  date: Date
  type: MessageType
}

interface IRoom {
  room: string
  lastMessage: string
  date: Date
  members: string[]
}

server.post(
  '/api/newuser',
  async (request: fastify.FastifyRequest, reply: fastify.FastifyReply<ServerResponse>) => {
    server.log.info('SOCKETSS CONNECTED')
    const name = request.body
    server.log.info('api/room body.name =', name)
    const res = await db.collection('users').findOne({ name })
    if (res) {
      reply.status(202)
      return { err: `${name} already created` }
    }
    const id = nanoidGenerate('1234567890abcdefghijklmnoprstuvwxyz', 10)
    await db.collection('users').insertOne({ name, rooms: [id] })
    await db.collection('rooms').insertOne({
      _id: id,
      messages: [],
      members: [name],
    })
    reply.status(201)
    return { id }
  }
)

server.get(
  '/api/roomlist',
  async (request: fastify.FastifyRequest, reply: fastify.FastifyReply<ServerResponse>) => {
    const name = request.query.name
    server.log.info('api/roomlist name', name)
    const userRoomsCursor = db.collection('users').aggregate([
      { $match: { name } },
      { $unwind: '$rooms' },
      {
        $lookup: {
          from: 'rooms',
          localField: 'rooms',
          foreignField: '_id',
          as: 'roomlist',
        },
      },
      { $unwind: '$roomlist' },
    ])
    let roomlist: IRoom[] = []
    let res = await userRoomsCursor.toArray()
    server.log.info('userRoomsCursor.toArray() =', res)
    if (res.length === 0) {
      return { err: 'no messages' }
    }
    res.forEach(doc => {
      server.log.info('userRoomsCursor ', JSON.stringify(doc, null, 2))
      const rl = doc.roomlist
      roomlist.push({
        room: doc.rooms,
        lastMessage: rl.messages.length ? rl.messages[rl.messages.length - 1] : '',
        date: rl.messages.length ? rl.messages[rl.messages.length - 1].date : '',
        members: rl.members,
      })
    })
    server.log.info('roomlist =', roomlist)
    return roomlist
  }
)

function getNotChip(messagelist: IMessage[]): IMessage | undefined {
  server.log.info('getNotChip', messagelist)
  for (let message of messagelist) {
    if (message.type === MessageType.MESSAGE) {
      return message
    }
  }
  return undefined
}

server.get(
  '/api/checkroom',
  async (request: fastify.FastifyRequest, reply: fastify.FastifyReply<ServerResponse>) => {
    const room = request.query.room
    let res = undefined
    try {
      res = await db.collection('rooms').findOne({ _id: room })
      if (!res) {
        server.log.error('/api/checkroom/ no such room', res)
        return { err: 'no such room' }
      }
    } catch (e) {
      server.log.error('api/checkroom', e)
      return { err: e }
    }
    server.log.info('/api/checkroom res = ', res)
    const message = getNotChip(res.messages)
    return {
      res: {
        room: res._id,
        lastMessage: message ? message : '',
        date: message ? message.date : '',
        members: res.members,
      },
    }
  }
)

async function getRoom(room: string) {
  const doc = await db.collection('rooms').findOne({ _id: room })
  if (!doc) {
    server.log.warn('doc is null')
    return
  }
  server.log.info('doc =', doc)
  return { room: doc }
}

io.on('connection', socket => {
  socket.on('joinroom', async (room: string, name: string) => {
    server.log.info('joinroom', room, name)
    socket.join(room, async () => {
      // server.log.info('socket join', room)
      server.log.info('socket join room =', Object.keys(socket.rooms))
      try {
        let res = await db.collection('rooms').findOne({ _id: room, members: name })
        server.log.info('room with member', res)
        if (!res) {
          const chipMessage = {
            _id: new mongo.ObjectId(),
            name,
            text: `${name} connected to ${room} room`,
            data: new Date(),
            type: MessageType.CHIP,
          }
          res = await db.collection('rooms').updateOne(
            { _id: room },
            {
              $push: {
                messages: chipMessage,
              },
            }
          )
          if (!res) {
            server.log.error('cannot update messages', res)
            return
          }
          socket.broadcast.to(room).emit('info message', chipMessage)
          server.log.info('new member', res)
          res = await db.collection('rooms').updateOne({ _id: room }, { $push: { members: name } })
          if (!res.result.ok) {
            server.log.error('rooms updateone executed incorrectly', res)
            return
          }
          res = await db.collection('users').updateOne({ name }, { $push: { rooms: room } })
          if (!res.result.ok) {
            server.log.error('users updateOne executed incorrectly', res)
            return
          }
          return
        }
        const roomContent = await getRoom(room)
        io.to(room).emit(
          'connect to room',
          `${name} joined the room ${room}`,
          roomContent.room.messages,
          name,
          roomContent.room.members
        )
      } catch (e) {
        server.log.error(e.stack)
        return
      }
    })
  })
  socket.on('say to room', async (room: string, name: string, msg: string) => {
    const message = {
      _id: new mongo.ObjectId(),
      name,
      text: msg,
      date: new Date(),
      type: MessageType.MESSAGE,
    }
    server.log.info('say to room', message)
    try {
      const res = await db
        .collection('rooms')
        .updateOne({ _id: room }, { $push: { messages: message } })
      if (!res) {
        server.log.error('cannot update messages', res)
        return
      }
      server.log.info('say to room res =', res)
    } catch (e) {
      server.log.error(e)
    }
    server.log.info('joined rooms =', Object.keys(socket.rooms))
    socket.broadcast.to(room).emit('send message', message)
  })
  socket.on('remove chat', async (room: string, name: string) => {
    server.log.info('socket id', socket.id)
    server.log.info('remove chat', room, name)
    try {
      let res = await db.collection('users').updateOne({ name }, { $pull: { rooms: room } })
      server.log.info('remove chat res', res)
      if (res) {
        res = await db
          .collection('rooms')
          .updateOne({ _id: room, members: name }, { $pull: { members: name } })
        server.log.info('remove chat res', res)
        if (!res) {
          server.log.error('cant delete room')
          return
        }
      } else {
        server.log.error('cant delete room')
        return
      }
    } catch (err) {
      server.log.error('remove chat exception', err)
      return
    }
    let test = await db.collection('users').findOne({ name })
    server.log.info('db test', test)
    try {
      if (Object.keys(socket.rooms).length < 2) {
        server.log.info('send to myself', socket.rooms)
        socket.broadcast.emit('chat removed', room, name)
      } else {
        socket.leave(room, () => {
          server.log.info('socket rooms2', socket.rooms)
          socket.broadcast.to(room).emit('chat removed', room, name)
        })
      }
      server.log.info('remove chat user removed from room')
    } catch (e) {
      server.log.error('socket', e)
    }
  })
})

dbClient.connect(err => {
  if (err) {
    server.log.error(err)
    process.exit(1)
  }
  db = dbClient.db(dbName)
  server.listen(3001, err => {
    if (err) {
      server.log.error(err)
      process.exit(1)
      dbClient.close()
    }
  })
})
// Run the server!
