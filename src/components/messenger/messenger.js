import React, { useCallback, useState, useContext, useEffect } from 'react'
import { BrowserRouter as Router, Redirect } from 'react-router-dom'
import { AppContext } from '../../contexts/AppContext'
import Layout from '../layout/layout'
import RoomList from '../roomList/roomList'
import MessageList from '../messageList/messageList'
import MessageInput from '../messageInput/messageInput'
import RoomInput from '../roomInput/roomInput'
import RoomInfo from '../roomInfo/roomInfo'
import io from 'socket.io-client'
import { RoomContext } from '../../contexts/RoomContext'
import { CHIP_TYPE } from '../../constants'

function Messenger({ location }) {
  const appctx = useContext(AppContext)
  const [roomList, setRoomList] = useState([])
  const [currRoom, setCurrRoom] = useState('')
  const [messageList, setMessageList] = useState([])
  // const [members, setMembers] = useState([])
  const [currSection, setCurrSection] = useState('roomlist')

  const getRoomList = useCallback(async () => {
    const res = await fetch(`${appctx.addr}/api/roomlist?name=${appctx.name}`)
    const json = await res.json()
    let roomlist = []
    if (json.err) {
      console.log(json.err)
      setRoomList([])
      setMessageList([])
      return
    }
    json.forEach(doc => {
      roomlist.push({
        room: doc.room,
        lastMessage: doc.lastMessage,
        date: doc.date,
        members: doc.members,
      })
    })
    setRoomList(roomlist)
  }, [appctx.name])

  useEffect(() => {
    if (appctx.name.length < 1) {
      return <Redirect to="/" />
    }
    getRoomList()
    if (location.state.room.length === 0) {
      getRoomList()
      return
    }
    setRoomList([{ room: location.state.room, lastMessage: '', date: '' }])
  }, [location.state.room, appctx.name, location, getRoomList])

  useEffect(() => {
    if (currRoom === '') {
      return
    }
    function connectToRoomCb(msg, ml, name, members) {
      const newml = ml.filter(message => {
        if(message.type === CHIP_TYPE && message.name === appctx.name){
          return false
        }
        return true
      })
      setMessageList(newml)
    }
    const socket = io(appctx.addr)
    socket.emit('joinroom', currRoom, appctx.name)
    socket.on('connect to room', connectToRoomCb)

    // TODO info message
    function infoMessageCb(message) {
      if (message.name !== appctx.name)
        setMessageList(messageList => [...messageList, message])
    }
    socket.on('info message', infoMessageCb)

    function sendMessageCb(message) {
      setMessageList(messageList => [...messageList, message])
      getRoomList()
    }
    socket.on('send message', sendMessageCb)

    return () => {
      socket.off('connect to room', connectToRoomCb)
      socket.off('send message', sendMessageCb)
      socket.off('info message', infoMessageCb)
    }
  }, [currRoom, appctx.name, getRoomList, appctx.addr])

  function addRoom(room) {
    const newroom = {
      room: room.room,
      lastMessage: room.lastMessage || '',
      date: room.date || '',
      members: room.members,
    }
    setRoomList([...roomList, newroom])
  }

  useEffect(() => {
    function deleteRoomCb(room, name) {
      console.warn('deleteRoomCb', room, name)
      if (currRoom === room && name === appctx.name) {
        setCurrRoom('')
        setMessageList([])
      }
      getRoomList()
    }
    const socket = io(appctx.addr)
    socket.on('chat removed', deleteRoomCb)
    return () => {
      socket.off('chat removed', deleteRoomCb)
    }
  }, [currRoom, getRoomList, appctx])

  return (
    <RoomContext.Provider
      value={{ currRoom, setCurrRoom, setCurrSection }}
    >
      <Layout>
        <div className="bx--row">
          <div
            className={`rooms-container ${
              currSection === 'roomlist' ? 'active' : ''
            } bx--col-sm-8 bx--col-md-7 bx--col-lg-7 bx--col-xlg-7`}
          >
            <RoomList list={roomList} />
            <RoomInput list={roomList} addRoom={addRoom} />
          </div>
          <div
            className={`messages-container ${
              currSection === 'messageslist' ? 'active' : ''
            } bx--col-sm-8 bx--col-md-9 bx--col-lg-9 bx--col-xlg-9`}
          >
            <RoomInfo room={currRoom} roomList={roomList} />
            <MessageList list={messageList} />
            <MessageInput />
          </div>
        </div>
      </Layout>
    </RoomContext.Provider>
  )
}
export default Messenger
