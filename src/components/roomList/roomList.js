import React from 'react'
import Room from '../room/room'

function RoomList({ list, deleteRoomProp }) {
  return (
    <div className="room-list">
      {list.length === 0 ? (
        <p className="no-rooms">find room below </p>
      ) : (
        list.map(v => (
          <Room
            deleteRoomProp={deleteRoomProp}
            room={v.room}
            lastMessage={v.lastMessage}
            date={v.date}
            members={v.members}
            key={v.room}
          />
        ))
      )}
    </div>
  )
}
export default RoomList
