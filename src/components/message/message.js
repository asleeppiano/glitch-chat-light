import React, { useContext } from 'react'
import { AppContext } from '../../contexts/AppContext'

function Message({ text, name, date }) {
  const appctx = useContext(AppContext)
  let d = new Date(date)
  return (
    <div className="message-container">
      <div
        className={`message ${
          name === appctx.name ? 'message-sender' : 'message-recipient'
        }`}
      >
        <span className="message-user">{name}</span>
        <p className="message-text">{text}</p>
        <time className="message-time">
          {d.getHours()}:{d.getMinutes()}
        </time>
      </div>
    </div>
  )
}
export default Message
