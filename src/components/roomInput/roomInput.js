import React, { useState, useRef, useContext } from 'react'
import {
  InlineNotification,
  Form,
  TextInput,
  Button,
} from 'carbon-components-react'
import { AppContext } from '../../contexts/AppContext'
import { Add16 } from '@carbon/icons-react'

function capitalize(string) {
  return string[0].toUpperCase() + string.slice(1)
}

function RoomInput(props) {
  const tiRef = useRef(null)
  const [err, setError] = useState(null)
  const appctx = useContext(AppContext)

  async function addRoom(e) {
    e.preventDefault()
    if (props.list.find(el => el.room === tiRef.current.value)) {
      setTimeout(() => {
        setError(null)
      }, 2000)
      setError({ text: 'you are already joined this room', kind: 'error' })
      return
    }
    const res = await fetch(
      `${appctx.addr}/api/checkroom?room=${tiRef.current.value}`
    )
    const check = await res.json()
    if (check.err) {
      setTimeout(() => {
        setError(null)
      }, 2000)
      setError({ text: check.err, kind: 'error' })
      return
    }
    props.addRoom(check.res)
  }
  return (
    <div style={{ position: 'relative' }}>
      {err ? (
        <InlineNotification
          className="room-input-notification"
          role="alert"
          kind={err.kind}
          title={capitalize(err.kind)}
          subtitle={err.text}
        />
      ) : (
        ''
      )}
      <Form onSubmit={addRoom} className="flex">
        <TextInput
          ref={tiRef}
          labelText=""
          id="room-input"
          className="room-input"
          type="text"
          placeholder="roomId"
        />
        <Button type="submit" kind="primary">
          <Add16 className="add-btn" aria-label="Add" />
        </Button>
      </Form>
    </div>
  )
}
export default RoomInput
