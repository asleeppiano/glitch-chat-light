import React, { useEffect, useRef, useContext } from 'react'
import Message from '../message/message'
import { RoomContext } from '../../contexts/RoomContext'
import MessageChip from '../messageChip/messageChip'
import { CHIP_TYPE } from '../../constants'

function Messagelist({ list }) {
  const roomctx = useContext(RoomContext)
  const messageListRef = useRef(null)
  console.log('MESSAGELIST', list)
  useEffect(() => {
    if (list.length === 0) {
      return
    }
    if (messageListRef.current) {
      messageListRef.current.scrollTop = messageListRef.current.scrollHeight
    }
  })
  return (
    <div
      style={
        roomctx.currRoom === ''
          ? { height: '100%' }
          : { height: 'calc(100% - 7.2rem)' }
      }
      ref={messageListRef}
      className="message-list"
    >
      {list.length === 0 ? (
        <p className="no-messages">select room to start messaging</p>
      ) : (
        list.map(v => {
          if (v.type === CHIP_TYPE) {
            return <MessageChip key={v._id} text={v.text} />
          }
          return <Message key={v._id} text={v.text} name={v.name} date={v.date} />
        })
      )}
    </div>
  )
}
export default Messagelist
