import React, { useState, useContext, useRef } from 'react'
import {
  InlineNotification,
  TextInput,
  Form,
  Button,
} from 'carbon-components-react'
import { BrowserRouter as Router, Redirect } from 'react-router-dom'
import Layout from '../layout/layout'
import { AppContext } from '../../contexts/AppContext'

function Home(props) {
  const appctx = useContext(AppContext)
  const [redirect, setRedirect] = useState(false)
  const [room, setRoom] = useState('')
  const tiRef = useRef(null)
  const [err, setErr] = useState(null)

  async function submit(e) {
    e.preventDefault()
    let room = undefined
    try {
      const res = await fetch(`${appctx.addr}/api/newuser`, {
        method: 'POST',
        body: tiRef.current.value,
        mode: 'cors',
      })
      room = await res.json()
      if (room.err) {
        room.id = ''
      }
    } catch (e) {
      console.log(e)
      setErr({ text: e, kind: 'error' })
      return
    }
    appctx.setName(tiRef.current.value)
    setErr(null)
    setRoom(room.id)
    setRedirect(true)
  }
  if (redirect) {
    return (
      <Redirect
        to={{
          pathname: '/messenger',
          state: { room },
        }}
      />
    )
  }
  return (
    <Layout>
      <Form className="home-form bx--col-lg-8" onSubmit={submit}>
        <h2>Hello, what&#39;s your name?</h2>
        <TextInput
          className="home-text-input"
          ref={tiRef}
          placeholder="name"
          labelText=""
          type="text"
          id="name"
        />
        <Button type="submit">Start</Button>
      </Form>
      {err ? (
        <InlineNotification kind={err.kind} role="alert" title={err.text} />
      ) : (
        ''
      )}
    </Layout>
  )
}

export default Home
