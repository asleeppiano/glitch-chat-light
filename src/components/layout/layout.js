import React from 'react'
import { Header, HeaderName } from 'carbon-components-react'

function Layout(props) {
  return (
    <React.Fragment>
      {window.location.pathname === '/' ? (
        <Header aria-label="header">
          <HeaderName href="/" prefix="">
            Glitch Messenger
          </HeaderName>
        </Header>
      ) : (
        ''
      )}
      <div className="content bx--grid">{props.children}</div>
    </React.Fragment>
  )
}
export default Layout
