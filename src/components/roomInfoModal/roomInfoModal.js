import React, { useContext } from 'react'
import { Modal, UnorderedList, ListItem } from 'carbon-components-react'
import { RoomContext } from '../../contexts/RoomContext'

function RoomInfoModal({ open, modal, members }) {
  const roomctx = useContext(RoomContext)
  return (
    <Modal
      onRequestClose={modal}
      modalAriaLabel="room info"
      passiveModal={true}
      open={open}
      modalHeading={`Room: ${roomctx.currRoom}`}
    >
      <h2 className="room-info-modal-heading">Room members:</h2>
      <UnorderedList>
        {members.map(v => (
          <ListItem key={v}>{v}</ListItem>
        ))}
      </UnorderedList>
    </Modal>
  )
}
export default RoomInfoModal
