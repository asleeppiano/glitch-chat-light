import React, { useRef, useContext } from 'react'
import io from 'socket.io-client'
import { Form, TextInput, Button } from 'carbon-components-react'
import { Add16 } from '@carbon/icons-react'
import { RoomContext } from '../../contexts/RoomContext'
import { AppContext } from '../../contexts/AppContext'

function MessageInput(props) {
  const tiRef = useRef(null)
  const roomctx = useContext(RoomContext)
  const appctx = useContext(AppContext)

  function sendMessage(e) {
    e.preventDefault()
    const socket = io(appctx.addr)
    socket.emit(
      'say to room',
      roomctx.currRoom,
      appctx.name,
      tiRef.current.value
    )
  }

  return (
    <React.Fragment>
      {roomctx.currRoom !== '' ? (
        <Form onSubmit={sendMessage} className="flex">
          <TextInput
            ref={tiRef}
            id="message-input"
            labelText=""
            className="message-input"
            type="text"
            placeholder="message"
          />
          <Button type="submit" kind="primary">
            <Add16 className="add-btn" aria-label="Add" />
          </Button>
        </Form>
      ) : (
        ''
      )}
    </React.Fragment>
  )
}
export default MessageInput
