import React, { useContext, useState } from 'react'
import { Button } from 'carbon-components-react'
import { ArrowLeft20 } from '@carbon/icons-react'
import { RoomContext } from '../../contexts/RoomContext'
import RoomInfoModal from '../roomInfoModal/roomInfoModal'

function RoomInfo({ room, roomList }) {
  const roomctx = useContext(RoomContext)
  const [openModal, setOpenModal] = useState(false)
  let members = []
  if(roomList){
    const res = roomList.find(room => room.room === roomctx.currRoom)
    members = res ? res.members : []
  }
  function back(e) {
    e.preventDefault()
    roomctx.setCurrSection('roomlist')
  }
  function modal(e) {
    e.preventDefault()
    setOpenModal(openModal => (openModal ? false : true))
  }
  return (
    <React.Fragment>
      <RoomInfoModal members={members} modal={modal} open={openModal} />
      {room ? (
        <div className="room-info-container">
          <Button
            onClick={back}
            tooltipPosition="bottom"
            tooltipAlignment="center"
            iconDescription="back"
            className="room-info-back-btn"
            kind="secondary"
            renderIcon={ArrowLeft20}
            size="default"
            hasIconOnly
          />
          <div onClick={modal} className="room-info">
            <h3 className="room-info-name">{room}</h3>
            <p className="room-members-count">
              {members.length ? `${members.length} members` : ''}
            </p>
          </div>
        </div>
      ) : (
        ''
      )}
    </React.Fragment>
  )
}
export default RoomInfo
