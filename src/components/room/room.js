import React, { useState, useContext } from 'react'
import { RoomContext } from '../../contexts/RoomContext'
import { AppContext } from '../../contexts/AppContext'
import { Copy16, Close20 } from '@carbon/icons-react'
import io from 'socket.io-client'

function Room({ img, room, lastMessage, date, members, deleteRoomProp }) {
  let datefmt = undefined
  if (date !== '') {
    let d = new Date(date)
    datefmt = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`
  } else {
    datefmt = 'no date'
  }

  const roomctx = useContext(RoomContext)
  const appctx = useContext(AppContext)
  const [copied, setCopied] = useState(false)

  function copyToClipboard(e) {
    const button = e.target.closest('.copy-tooltip-container')
    e.stopPropagation()
    navigator.clipboard
      .writeText(room)
      .then(() => {
        button.blur()

        setCopied(true)
        setTimeout(() => {
          setCopied(false)
        }, 1500)
      })
      .catch(err => console.log('err', err))
  }

  function selectChat() {
    if (window.innerWidth < 1056) {
      roomctx.setCurrSection(currSection =>
        currSection === 'roomlist' ? 'messageslist' : 'roomlist'
      )
    }
    roomctx.setCurrRoom(room)
  }

  function deleteRoom(e) {
    e.preventDefault()
    e.stopPropagation()
    const socket = io(appctx.addr)
    socket.emit('remove chat', room, appctx.name)
  }

  return (
    <div
      onClick={selectChat}
      className={`room room-grid ${
        roomctx.currRoom === room ? 'room-selected' : ''
      }`}
    >
      {img ? (
        <img className="room-grid-img" src={img} alt="chat_image" />
      ) : (
        <div className="room-grid-img"></div>
      )}
      <div className="flex room-grid-title">
        <h3 className="room-title">{room}</h3>
        <div
          onClick={copyToClipboard}
          className={`copy-tooltip-container ${copied ? 'click' : ''}`}
          aria-label="copied"
        >
          <Copy16 className="copy-btn" aria-label="copy" />
          <div className={`copy-tooltip-arrow ${copied ? 'click' : ''}`}></div>
          <div className={`copy-tooltip ${copied ? 'click' : ''}`}>
            <span>copied</span>
          </div>
        </div>
      </div>
      <div className="room-grid-lastmsg">
        {lastMessage === '' || !lastMessage ? (
          ''
        ) : (
          <React.Fragment>
            <span className="room-name">{lastMessage.name}:</span>
            <span className="room-text">{lastMessage.text}</span>
          </React.Fragment>
        )}
      </div>
      <time dateTime={datefmt} className="room-grid-date">
        {datefmt}
      </time>
      <Close20
        onClick={deleteRoom}
        className="room-grid-close room-delete"
        aria-label="delete"
      />
    </div>
  )
}
export default Room
