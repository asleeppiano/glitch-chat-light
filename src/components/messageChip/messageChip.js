import React from 'react'
import { Tag } from 'carbon-components-react'

function MessageChip({ text }) {
  return (
    <div className="message-chip-container">
      <Tag className="message-chip" type="cool-gray">
        {text}
      </Tag>
    </div>
  )
}
export default MessageChip
