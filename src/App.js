import React, { useState } from 'react'
import './App.scss'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './components/home/home'
import Messenger from './components/messenger/messenger'
import { AppContext } from './contexts/AppContext'

function App() {
  const addr = 'http://localhost:3001'
  const [name, setName] = useState('')
  return (
    <div className="App">
      <AppContext.Provider value={{ name, setName, addr }}>
        <Router>
          <Route exact path="/" component={Home} />
          <Route path="/messenger" component={Messenger} />
        </Router>
      </AppContext.Provider>
    </div>
  )
}

export default App
