import { configure } from '@storybook/react';
import '../src/App.scss'
import RoomContext from '../src/contexts/RoomContext'

function loadStories() {
  require('../stories/index.js');
  // You can require as many stories as you need.
}

configure(loadStories, module);
