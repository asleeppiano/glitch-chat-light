const path = require('path')

module.exports = async ({config, mode}) => {
  config.module.rules.push({
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          { loader: "css-loader", options: { importLoaders: 1 } },
          "sass-loader"
        ]
  })
  return config
}
